 select *
    from product pro
    inner join spectacle spe on pro.sec_spectacle = spe.sec_spectacle
    where REGEXP_LIKE(upper(spe.name),upper('rus'))
    
    /
    
    /*
    Ranking de los productos más vendidos en un rango de fechas dado, es decir, los productos
     ordenados desde el que más ha participado en órdenes de pedido
    */
  select *
  from(
  select 
          count(0)
          ,code
          ,name
         ,description
         ,spectacle_date
    from product pro
    where spectacle_date between to_date(:p_ini,'dd/mm/yyyy') and to_date(:p_fin,'dd/mm/yyyy') 
    group by
          code
          ,name
          ,description
          ,spectacle_date
  order by 1 desc
  )where rownum<=5
    
    