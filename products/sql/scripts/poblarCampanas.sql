declare
        sec number(12) := 1;
    begin 
        for c_prod in (
            select pro.SEC_PRODUCT
            from product pro
            where    sec_product between 10000 and 10200 
        )
        loop
            insert into detail_campana(sec_detail_campana,sec_campana,sec_product,description,status)
            values(sec,1,c_prod.sec_product,dbms_random.string('X', 20),'C' );
            sec := sec +1;
        end loop;
        commit;
    end;