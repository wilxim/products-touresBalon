--------------------------------------PRODUCT-----
create  index product_i1 on product(code)
TABLESPACE lmtbsb
      STORAGE (INITIAL 20K
      NEXT 20k
      PCTINCREASE 75);
     /
     create  index product_i2 on product(name)
TABLESPACE tbProducts
      STORAGE (INITIAL 20K
      NEXT 20k
      PCTINCREASE 75);
      
      /
      
      create  index product_i3 on product(description)
TABLESPACE tbProducts
      STORAGE (INITIAL 20K
      NEXT 20k
      PCTINCREASE 75);
      /
      ---------------------------------SPECTACLE
      /
           create  index spectacle_i1 on spectacle(name)
TABLESPACE tbSpectacle
      STORAGE (INITIAL 20K
      NEXT 20k
      PCTINCREASE 75);
      /
      