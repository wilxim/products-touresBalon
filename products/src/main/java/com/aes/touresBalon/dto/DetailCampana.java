package com.aes.touresBalon.dto;

public class DetailCampana {
	
	private double	secDetailCampana;
	private Campana	campana;
	private double	secProduct;
	private String	description;
	private String	status;
	private Product product;
	
	
	
	
	
	
	public Product getProduct() {
		return product;
	}
	public void setProduct(Product product) {
		this.product = product;
	}
	public double getSecDetailCampana() {
		return secDetailCampana;
	}
	public void setSecDetailCampana(double secDetailCampana) {
		this.secDetailCampana = secDetailCampana;
	}
	public Campana getCampana() {
		return campana;
	}
	public void setCampana(Campana campana) {
		this.campana = campana;
	}
	public double getSecProduct() {
		return secProduct;
	}
	public void setSecProduct(double secProduct) {
		this.secProduct = secProduct;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	
	

}
