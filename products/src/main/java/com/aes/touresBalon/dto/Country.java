package com.aes.touresBalon.dto;

public class Country {
	
	private Long	secCountry;
	private String	name;
	
	
	public Long getSecCountry() {
		return secCountry;
	}
	public void setSecCountry(Long secCountry) {
		this.secCountry = secCountry;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	
	

}
