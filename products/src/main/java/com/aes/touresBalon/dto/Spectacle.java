package com.aes.touresBalon.dto;

public class Spectacle {
	
	private Long	secSpectacle;
	private String	name;
	private Long	cost;
	
	
	public Long getSecSpectacle() {
		return secSpectacle;
	}
	public void setSecSpectacle(Long secSpectacle) {
		this.secSpectacle = secSpectacle;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Long getCost() {
		return cost;
	}
	public void setCost(Long cost) {
		this.cost = cost;
	}
	
	
	

}
