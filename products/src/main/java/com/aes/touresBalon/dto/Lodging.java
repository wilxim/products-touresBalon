package com.aes.touresBalon.dto;

public class Lodging {
	
	private Long	secLodging;
	private String	name;
	private Long	cost;
	
	
	public Long getSecLodging() {
		return secLodging;
	}
	public void setSecLodging(Long secLodging) {
		this.secLodging = secLodging;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Long getCost() {
		return cost;
	}
	public void setCost(Long cost) {
		this.cost = cost;
	}
	
	

}
