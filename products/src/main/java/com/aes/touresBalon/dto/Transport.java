package com.aes.touresBalon.dto;

public class Transport {
	
	private Long	secTransport;
	private String	name;
	private Long	cost;
	public Long getSecTransport() {
		return secTransport;
	}
	public void setSecTransport(Long secTransport) {
		this.secTransport = secTransport;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Long getCost() {
		return cost;
	}
	public void setCost(Long cost) {
		this.cost = cost;
	}
	
	
	

}
