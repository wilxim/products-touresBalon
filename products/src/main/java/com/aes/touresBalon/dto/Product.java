package com.aes.touresBalon.dto;

import java.sql.Blob;
import java.util.Date;

public class Product {
	
	private Long			secProduct;
	private Lodging		lodging;
	private Spectacle	spectacle;
	private Transport	transport;
	private City	   		citySource;
	private City 		cityTarget;
	private String		name;
	private Date			spectacleDate;
	private Date			arrivalDate;
	private Date			departureDate;
	private String		description;
	private String		code;
	private 	Blob   		imageRef;
	public Long getSecProduct() {
		return secProduct;
	}
	public void setSecProduct(Long secProduct) {
		this.secProduct = secProduct;
	}
	public Lodging getLodging() {
		return lodging;
	}
	public void setLodging(Lodging lodging) {
		this.lodging = lodging;
	}
	public Spectacle getSpectacle() {
		return spectacle;
	}
	public void setSpectacle(Spectacle spectacle) {
		this.spectacle = spectacle;
	}
	public Transport getTransport() {
		return transport;
	}
	public void setTransport(Transport transport) {
		this.transport = transport;
	}
	public City getCitySource() {
		return citySource;
	}
	public void setCitySource(City citySource) {
		this.citySource = citySource;
	}
	public City getCityTarget() {
		return cityTarget;
	}
	public void setCityTarget(City cityTarget) {
		this.cityTarget = cityTarget;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Date getSpectacleDate() {
		return spectacleDate;
	}
	public void setSpectacleDate(Date spectacleDate) {
		this.spectacleDate = spectacleDate;
	}
	public Date getArrivalDate() {
		return arrivalDate;
	}
	public void setArrivalDate(Date arrivalDate) {
		this.arrivalDate = arrivalDate;
	}
	public Date getDepartureDate() {
		return departureDate;
	}
	public void setDepartureDate(Date departureDate) {
		this.departureDate = departureDate;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public Blob getImageRef() {
		return imageRef;
	}
	public void setImageRef(Blob imageRef) {
		this.imageRef = imageRef;
	}
	


}
