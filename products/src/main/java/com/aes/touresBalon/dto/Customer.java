package com.aes.touresBalon.dto;


import java.util.List;

public class Customer {
	
	private Long   	secCustomer;
	private String	firstName;
	private String	lastName;
	private String	phoneNumber;
	private String	email;
	private String	password;
	private String	status;
	private String identificacion;
	
	
	
	
	
	public String getIdentificacion() {
		return identificacion;
	}
	public void setIdentificacion(String identificacion) {
		this.identificacion = identificacion;
	}
	public Long getSecCustomer() {
		return secCustomer;
	}
	public void setSecCustomer(Long secCustomer) {
		this.secCustomer = secCustomer;
	}
	
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	
	
	
}
