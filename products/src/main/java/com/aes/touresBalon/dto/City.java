package com.aes.touresBalon.dto;

public class City {
	
	private Long	secCity;
	private Country	country;
	private String	name;
	private Long	cost;
	
	
	public Long getSecCity() {
		return secCity;
	}
	public void setSecCity(Long secCity) {
		this.secCity = secCity;
	}
	public Country getCountry() {
		return country;
	}
	public void setCountry(Country country) {
		this.country = country;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Long getCost() {
		return cost;
	}
	public void setCost(Long cost) {
		this.cost = cost;
	}
	
	

}
