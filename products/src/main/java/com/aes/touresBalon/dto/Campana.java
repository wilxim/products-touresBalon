package com.aes.touresBalon.dto;

import java.util.Date;

public class Campana {
	
	private Long	 secCampana;
	private String	description;
	private String	apertureDate;
	private String	closeDate;
	private String	status;
	private Integer cantidadDetalle;
	
	
	
	
	
	public Integer getCantidadDetalle() {
		return cantidadDetalle;
	}
	public void setCantidadDetalle(Integer cantidadDetalle) {
		this.cantidadDetalle = cantidadDetalle;
	}
	public double getSecCampana() {
		return secCampana;
	}
	public void setSecCampana(Long secCampana) {
		this.secCampana = secCampana;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getApertureDate() {
		return apertureDate;
	}
	public void setApertureDate(String apertureDate) {
		this.apertureDate = apertureDate;
	}
	public String getCloseDate() {
		return closeDate;
	}
	public void setCloseDate(String closeDate) {
		this.closeDate = closeDate;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	

}
