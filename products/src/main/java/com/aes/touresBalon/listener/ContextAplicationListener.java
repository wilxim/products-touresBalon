package com.aes.touresBalon.listener;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.TimeZone;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.apache.commons.dbcp.BasicDataSource;



public class ContextAplicationListener implements ServletContextListener{
	
	private static BasicDataSource ds;

	public void contextDestroyed(ServletContextEvent arg0) {
		// TODO Auto-generated method stub
		System.out.println("Ejecutando metodo [contextDestroyed]");
		
	}

	public void contextInitialized(ServletContextEvent arg0) {
		// TODO Auto-generated method stub
		System.out.println("Ejecutando metodo [contextInitialized]");
		
		 TimeZone timeZone = TimeZone.getTimeZone("America/Bogota");
	     TimeZone.setDefault(timeZone);
		 
		 ds = new BasicDataSource();
	        ds.setDriverClassName("oracle.jdbc.driver.OracleDriver");
	        ds.setUsername("toresBalonProduct");
	        ds.setPassword("toresBalonProduct");
	        ds.setUrl("jdbc:oracle:thin:@localhost:49166:xe");
	       
	     // the settings below are optional -- dbcp can work with defaults
	        ds.setMinIdle(5);
	        ds.setMaxIdle(20);
	        ds.setMaxOpenPreparedStatements(180);
	        
	     
	        
//	        try {
//				Connection connection = ds.getConnection();
//				PreparedStatement pstmt = connection.prepareStatement("SELECT SEC_PARAMETRO,COMPONENTE,PARAMETRO FROM get_parametro");
//				
//				ResultSet rs = pstmt.executeQuery();
//				rs = pstmt.executeQuery();
//				while (rs.next()) {
//					String userid = rs.getString("SEC_PARAMETRO");
//					String username = rs.getString("COMPONENTE");
//					System.out.println("SEC_PARAMETRO : " + userid);
//					System.out.println("COMPONENTE : " + username);
//			      }
//				
//			} catch (SQLException e) {
//				// TODO Auto-generated catch block
//				System.out.println("SQLException");
//				e.printStackTrace();
//			}
//	        
	        
	       
	}

	
	
	
	 public static BasicDataSource getDs() {
		return ds;
	}

	public static void setDs(BasicDataSource ds) {
		ContextAplicationListener.ds = ds;
	}

	public Connection getConnection() throws SQLException {
         return this.ds.getConnection();
     }
	 
	 
	public Connection GETConexionApiRest() throws ClassNotFoundException {
		System.out.println("Ejecutando metodo []GETConexionApiRest");
		Connection conn = null;
		
		try {
			Class.forName("oracle.jdbc.driver.OracleDriver");
			
			conn = DriverManager.getConnection(
			        "jdbc:oracle:thin:@localhost:49166:xe", "toresBalonProduct", "toresBalonProduct");
					 //"jdbc:oracle:thin:@soporte.casewaresa.com:1521:soporte", "toresBalonClientes", "toresBalonClientes");
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return conn;
	}
	
	
	

}
