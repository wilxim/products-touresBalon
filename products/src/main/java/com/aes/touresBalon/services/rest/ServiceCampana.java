package com.aes.touresBalon.services.rest;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.commons.dbcp.BasicDataSource;

import com.aes.touresBalon.dto.Campana;
import com.aes.touresBalon.dto.City;
import com.aes.touresBalon.dto.DetailCampana;
import com.aes.touresBalon.dto.Lodging;
import com.aes.touresBalon.dto.Product;
import com.aes.touresBalon.dto.Spectacle;
import com.aes.touresBalon.dto.Transport;
import com.aes.touresBalon.listener.ContextAplicationListener;
import com.sun.xml.ws.client.ClientSchemaValidationTube;


@Path("/campana")
public class ServiceCampana {
	
	private List<Campana> lstCampana;
	private List<DetailCampana> lstDetailCampana;
	private Campana campana;
	private List<Product> lstProduct;
	private Product product;
	private String strQuery;
	private ContextAplicationListener contApplList = new ContextAplicationListener();
	private Connection conn = null;
	private DetailCampana detailCampana;
	private String ejecProc;
	
	@GET
	@Path("/getAllCampana")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public List<Campana> getAllProduct(){
		System.out.println("Ejecutando metodo[getAllCampana]");
		lstCampana = new ArrayList<Campana>();
		
		strQuery =  "select \n" + 
					"    cam.sec_campana\n" + 
					"    ,cam.description\n" + 
					"    ,cam.aperture_Date\n" + 
					"    ,cam.close_date\n" + 
					"    ,cam.status\n" + 
					"    ,(\n" + 
					"        select count(0)\n" + 
					"        from detail_campana dc\n" + 
					"        where dc.sec_campana = cam.sec_campana\n" + 
					"    )cantidad_detalle\n" + 
					"from campana cam";
		
		System.out.println(strQuery);
		try {
			if (contApplList == null)
				contApplList = new ContextAplicationListener();

			BasicDataSource dbs;
			
			dbs = contApplList.getDs();
			
			//dbs.getConnection();
			Connection conn = dbs.getConnection();
			
			
			PreparedStatement pstmt = conn.prepareStatement(strQuery);
			//pstmt.executeQuery();
			ResultSet rs = pstmt.executeQuery();
			
			while (rs.next()) {
				campana = new Campana();
				
				campana.setSecCampana(rs.getLong("SEC_CAMPANA"));
				campana.setDescription(rs.getString("DESCRIPTION"));
				campana.setApertureDate(rs.getString("APERTURE_DATE"));
				campana.setCloseDate(rs.getString("CLOSE_DATE"));
				campana.setStatus(rs.getString("STATUS"));
				campana.setCantidadDetalle(rs.getInt("CANTIDAD_DETALLE"));
				
				lstCampana.add(campana);
		      }
			
			
			conn.close();
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

		
		
		return lstCampana;
		
	}

	
	
	@GET
	@Path("/getDetailCampana")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public List<DetailCampana> getDetailCampana(@QueryParam("secCampana")
												Long secCampana){
		System.out.println("Ejecutando metodo[getDetailCampana]");
		lstDetailCampana = new ArrayList<DetailCampana>();
		
		strQuery =  "select \n" + 
					"     dc.SEC_DETAIL_CAMPANA \n" + 
					"    ,dc.SEC_CAMPANA        \n" + 
					"    ,dc.SEC_PRODUCT        \n" + 
					"    ,pro.name NAME_PRODUCT\n" + 
					"    ,dc.DESCRIPTION        \n" + 
					"    ,dc.STATUS \n" + 
					"from DETAIL_CAMPANA dc\n" + 
					"inner join product pro on pro.sec_product = dc.sec_product\n" + 
					"where dc.SEC_CAMPANA =  "+secCampana;
		
		System.out.println(strQuery);
		try {
			if (contApplList == null)
				contApplList = new ContextAplicationListener();

			BasicDataSource dbs;
			
			dbs = contApplList.getDs();
			
			//dbs.getConnection();
			Connection conn = dbs.getConnection();
			
			
			PreparedStatement pstmt = conn.prepareStatement(strQuery);
			//pstmt.executeQuery();
			ResultSet rs = pstmt.executeQuery();
			
			while (rs.next()) {
				detailCampana = new DetailCampana();
//				
//				
				detailCampana.setSecDetailCampana(rs.getLong("SEC_CAMPANA"));
				detailCampana.setCampana(new Campana());
				detailCampana.getCampana().setSecCampana(rs.getLong("SEC_CAMPANA"));
				detailCampana.setProduct(new Product());
				detailCampana.getProduct().setSecProduct(rs.getLong("SEC_PRODUCT"));
				detailCampana.getProduct().setName("NAME_PRODUCT");
				detailCampana.setDescription(rs.getString("DESCRIPTION"));
				detailCampana.setStatus(rs.getString("STATUS"));				
//				
				lstDetailCampana.add(detailCampana);
		      }
			
			
			conn.close();
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

		
		
		return lstDetailCampana;
		
	}
	
	
	@GET
	@Path("/getProductDetCamBySecCampana")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public List<Product> getProductDetCamBySecCampana(@QueryParam("secCampana")
												Long secCampana){
		System.out.println("Ejecutando metodo[getDetailCampana]");
		System.out.println("[secCampana]:"+secCampana);
		
		lstProduct = new ArrayList<Product>();
		
		strQuery =  "select \n" + 
				"     pro.SEC_PRODUCT     \n" + 
				"    ,pro.SEC_LODGING     \n" + 
				"    ,lod.NAME as NAME_LODGING\n" + 
				"    ,pro.SEC_SPECTACLE   \n" + 
				"    ,spe.NAME As NAME_SPECTACLE\n" + 
				"    ,pro.SEC_TRANSPORT   \n" + 
				"    ,tra.NAME as NAME_TRANSPORT\n" + 
				"    ,pro.SEC_CITY_SOURCE \n" + 
				"    ,cityS.NAME AS CITY_SOURCE\n" + 
				"    ,pro.SEC_CITY_TARGET \n" + 
				"    ,cityT.NAME AS CITY_TARGET\n" + 
				"    ,pro.NAME            \n" + 
				"    ,pro.SPECTACLE_DATE  \n" + 
				"    ,pro.ARRIVAL_DATE    \n" + 
				"    ,pro.DEPARTURE_DATE  \n" + 
				"    ,pro.DESCRIPTION     \n" + 
				"    ,pro.CODE            \n" + 
				"    ,pro.IMAGE_REF   \n" + 
				"   \n" + 
				"from product pro\n" + 
				"inner join SPECTACLE spe on pro.SEC_SPECTACLE = pro.SEC_SPECTACLE\n" + 
				"inner join LODGING lod on pro.SEC_LODGING = lod.SEC_LODGING\n" + 
				"inner join transport tra on pro.sec_transport = tra.SEC_TRANSPORT\n" + 
				"inner join CITY cityS  on cityS.SEC_CITY =pro.SEC_CITY_SOURCE\n" + 
				"inner join CITY cityT  on cityT.SEC_CITY =pro.SEC_CITY_TARGET\n" + 
				"where exists (select null \n" + 
				"              from detail_campana dc \n" + 
				"              where dc.sec_campana= \n" +secCampana+ 
				"              and  pro.sec_product = dc.sec_product\n" + 
				"              )";
		
		System.out.println(strQuery);
		try {
			if (contApplList == null)
				contApplList = new ContextAplicationListener();

			BasicDataSource dbs;
			
			dbs = contApplList.getDs();
			
			//dbs.getConnection();
			Connection conn = dbs.getConnection();
			
			
			PreparedStatement pstmt = conn.prepareStatement(strQuery);
			//pstmt.executeQuery();
			ResultSet rs = pstmt.executeQuery();
			
			while (rs.next()) {
				product = new Product();
				
				product.setSecProduct(rs.getLong("SEC_PRODUCT"));
				product.setLodging(new Lodging());
				product.getLodging().setSecLodging(rs.getLong("SEC_LODGING"));
				product.getLodging().setName(rs.getString("NAME_LODGING"));
				
				product.setSpectacle(new Spectacle());
				product.getSpectacle().setSecSpectacle(rs.getLong("SEC_SPECTACLE"));
				product.getSpectacle().setName(rs.getString("NAME_SPECTACLE"));
				
				product.setTransport(new Transport());
				product.getTransport().setSecTransport(rs.getLong("SEC_TRANSPORT"));
				product.getTransport().setName(rs.getString("NAME_TRANSPORT"));
				
				product.setCitySource(new City());
				product.getCitySource().setSecCity(rs.getLong("SEC_CITY_SOURCE"));
				product.getCitySource().setName(rs.getString("CITY_SOURCE"));
				product.getCitySource().setCost(new Long(525));
				
				product.setCityTarget(new City());
				product.getCityTarget().setSecCity(rs.getLong("SEC_CITY_TARGET"));
				product.getCityTarget().setName(rs.getString("CITY_TARGET"));
				product.getCityTarget().setCost(new Long(85));
				product.setName(rs.getString("NAME"));
				product.setSpectacleDate(rs.getDate("SPECTACLE_DATE"));
				product.setArrivalDate(rs.getDate("ARRIVAL_DATE"));
				product.setDepartureDate(rs.getDate("DEPARTURE_DATE"));
				product.setDescription(rs.getString("DESCRIPTION"));
				product.setCode(rs.getString("CODE"));
				
						
				
				lstProduct.add(product);
		      }
			
			
			conn.close();
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

		
		
		return lstProduct;
		
	}
	
	@POST
	@Path("/createCampana")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response createCampana(Campana unCampana) {
		System.out.println("Ejecutando metodo[createCampana]");
		
		ejecProc = "declare\n" + 
				"                mi_secuencia number(12);\n" + 
				"              begin\n" + 
				"                select max(sec_campana) +1\n" + 
				"                into  mi_secuencia\n" + 
				"                from campana;\n" + 
				"                \n" + 
				"                insert into campana(SEC_CAMPANA, DESCRIPTION,APERTURE_DATE,CLOSE_DATE,STATUS)\n" + 
				"                values(mi_secuencia,?,?,?,?);\n" + 
				"                \n" + 
				"              end;";
		
		System.out.println(ejecProc);
		

			try {
				if (contApplList == null)
					contApplList = new ContextAplicationListener();

				BasicDataSource dbs;	
				dbs = contApplList.getDs();
				Connection conn = dbs.getConnection();
//				conn = contApplList.GETConexionApiRest();
				CallableStatement callAbleState;
				callAbleState = conn.prepareCall(ejecProc);
				
				//callAbleState.setLong(1, salOrder.getSecSalesOrder());
				callAbleState.setString(1, unCampana.getDescription());
				if(unCampana.getApertureDate() != null)
					callAbleState.setString(2, unCampana.getApertureDate().toString());
				else
					callAbleState.setString(2, "01/01/2017");
				
				if(unCampana.getCloseDate() != null)
					callAbleState.setString(3,unCampana.getCloseDate().toString());
				else
					callAbleState.setString(3,"01/01/2017");
				
				callAbleState.setString(4, unCampana.getStatus());
					
				callAbleState.executeUpdate();
				
				conn.close();
			
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
				StringWriter sw = new StringWriter();
				e.printStackTrace(new PrintWriter(sw));
				String exceptionAsString = sw.toString();
				
				
			}
			
			String result = "Track saved : " ;
			return Response.status(201).entity(result).build();
			
			
		
	}
	
		
	}
	
	
