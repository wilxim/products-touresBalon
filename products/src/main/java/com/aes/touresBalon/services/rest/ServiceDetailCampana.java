package com.aes.touresBalon.services.rest;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.commons.dbcp.BasicDataSource;

import com.aes.touresBalon.dto.Campana;
import com.aes.touresBalon.dto.DetailCampana;
import com.aes.touresBalon.dto.Product;
import com.aes.touresBalon.listener.ContextAplicationListener;

@Path("/detailCampana")
public class ServiceDetailCampana {

	private Product product;
	private List<Product> lstProduct;
	private String ejecProc;
	private ContextAplicationListener contApplList = new ContextAplicationListener();
	private Connection conn = null;
	
	
	
	
	@POST
	@Path("/createDetailCampana")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response createDetailCampana(DetailCampana unDetCampana) {
		System.out.println("Ejecutando metodo[createCampana]");
		
		ejecProc = "declare\n" + 
					"    miSecDetail number(12);\n" + 
					"begin\n" + 
					"    \n" + 
					"    select max(SEC_DETAIL_CAMPANA) +1\n" + 
					"    into miSecDetail\n" + 
					"    from DETAIL_CAMPANA;\n" + 
					"    \n" + 
					"    insert into DETAIL_CAMPANA(SEC_DETAIL_CAMPANA,SEC_CAMPANA,SEC_PRODUCT,DESCRIPTION,STATUS)\n" + 
					"    values(miSecDetail,?,?,?,?);\n" + 
					"end;";
		
		System.out.println(ejecProc);
		

			try {
				if (contApplList == null)
					contApplList = new ContextAplicationListener();

				BasicDataSource dbs;	
				dbs = contApplList.getDs();
				Connection conn = dbs.getConnection();
//				conn = contApplList.GETConexionApiRest();
				CallableStatement callAbleState;
				callAbleState = conn.prepareCall(ejecProc);
				
				//callAbleState.setLong(1, salOrder.getSecSalesOrder());
				callAbleState.setDouble(1, unDetCampana.getCampana().getSecCampana());
				callAbleState.setDouble(2, unDetCampana.getProduct().getSecProduct());
				callAbleState.setString(3, unDetCampana.getDescription());
				callAbleState.setString(4, unDetCampana.getStatus());
					
				callAbleState.executeUpdate();
				
				conn.close();
			
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
				StringWriter sw = new StringWriter();
				e.printStackTrace(new PrintWriter(sw));
				String exceptionAsString = sw.toString();
				
				
			}
			
			String result = "Track saved : " ;
			return Response.status(201).entity(result).build();
			
			
		
	}
	
}
