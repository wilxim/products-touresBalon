package com.aes.touresBalon.services.rest;

import java.sql.Blob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.apache.commons.dbcp.BasicDataSource;

import com.aes.touresBalon.dto.City;
import com.aes.touresBalon.dto.Lodging;
import com.aes.touresBalon.dto.Product;
import com.aes.touresBalon.dto.Spectacle;
import com.aes.touresBalon.dto.Transport;
import com.aes.touresBalon.listener.ContextAplicationListener;




@Path("/product")
public class ServiceProduct {
	
	private List<Product> lstProduct;
	private Product product;
	private String strQuery;
	private ContextAplicationListener contApplList = new ContextAplicationListener();
	private Connection conn = null;
	
	@GET
	@Path("/getAllProduct")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public List<Product> getAllProduct(){
		System.out.println("Ejecutando metodo[getAllProduct]");
		lstProduct = new ArrayList<Product>();
		
		strQuery = "select \r\n" + 
					"     pro.SEC_PRODUCT     \r\n" + 
					"    ,pro.SEC_LODGING     \r\n" + 
					"    ,lod.NAME as NAME_LODGING\r\n" + 
					"    ,pro.SEC_SPECTACLE   \r\n" + 
					"    ,spe.NAME As NAME_SPECTACLE\r\n" + 
					"    ,pro.SEC_TRANSPORT   \r\n" + 
					"    ,tra.NAME as NAME_TRANSPORT\r\n" + 
					"    ,pro.SEC_CITY_SOURCE \r\n" + 
					"    ,cityS.NAME AS CITY_SOURCE\r\n" + 
					"    ,pro.SEC_CITY_TARGET \r\n" + 
					"    ,cityT.NAME AS CITY_TARGET\r\n" + 
					"    ,pro.NAME            \r\n" + 
					"    ,pro.SPECTACLE_DATE  \r\n" + 
					"    ,pro.ARRIVAL_DATE    \r\n" + 
					"    ,pro.DEPARTURE_DATE  \r\n" + 
					"    ,pro.DESCRIPTION     \r\n" + 
					"    ,pro.CODE            \r\n" + 
					"    ,pro.IMAGE_REF       \r\n" + 
					"from product pro\r\n" + 
					"inner join SPECTACLE spe on pro.SEC_SPECTACLE = pro.SEC_SPECTACLE\r\n" + 
					"inner join LODGING lod on pro.SEC_LODGING = lod.SEC_LODGING\r\n" + 
					"inner join transport tra on pro.sec_transport = tra.SEC_TRANSPORT\r\n" + 
					"inner join CITY cityS  on cityS.SEC_CITY =pro.SEC_CITY_SOURCE\r\n" + 
					"inner join CITY cityT  on cityT.SEC_CITY =pro.SEC_CITY_TARGET\r\n" + 
					"where rownum< 100";
		
		System.out.println(strQuery);
		try {
			if (contApplList == null)
				contApplList = new ContextAplicationListener();

			BasicDataSource dbs;
			
			dbs = contApplList.getDs();
			
			dbs.getConnection();
			Connection conn = dbs.getConnection();
			
			
			PreparedStatement pstmt = conn.prepareStatement(strQuery);
			//pstmt.executeQuery();
			ResultSet rs = pstmt.executeQuery();
			
			while (rs.next()) {
				product = new Product();
				
				product.setSecProduct(rs.getLong("SEC_PRODUCT"));
				product.setLodging(new Lodging());
				product.getLodging().setSecLodging(rs.getLong("SEC_LODGING"));
				product.getLodging().setName(rs.getString("NAME_LODGING"));
				
				product.setSpectacle(new Spectacle());
				product.getSpectacle().setSecSpectacle(rs.getLong("SEC_SPECTACLE"));
				product.getSpectacle().setName(rs.getString("NAME_SPECTACLE"));
				
				product.setTransport(new Transport());
				product.getTransport().setSecTransport(rs.getLong("SEC_TRANSPORT"));
				product.getTransport().setName(rs.getString("NAME_TRANSPORT"));
				
				product.setCitySource(new City());
				product.getCitySource().setSecCity(rs.getLong("SEC_CITY_SOURCE"));
				product.getCitySource().setName(rs.getString("CITY_SOURCE"));
				
				product.setCityTarget(new City());
				product.getCityTarget().setSecCity(rs.getLong("SEC_CITY_TARGET"));
				product.getCityTarget().setName(rs.getString("CITY_TARGET"));
				product.setName(rs.getString("NAME"));
				product.setSpectacleDate(rs.getDate("SPECTACLE_DATE"));
				product.setArrivalDate(rs.getDate("ARRIVAL_DATE"));
				product.setDepartureDate(rs.getDate("DEPARTURE_DATE"));
				product.setDescription(rs.getString("DESCRIPTION"));
				product.setCode(rs.getString("CODE"));
				
						
				
				lstProduct.add(product);
		      }
			
			
			conn.close();
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

		
		
		return lstProduct;
		
	}
	
	
	
	@GET
	@Path("/getProductByCode")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public List<Product> getProductByCode(@QueryParam("code") String unCode){
		System.out.println("Ejecutando metodo[getProductByCode]");
		lstProduct = new ArrayList<Product>();
		
		strQuery = "select \r\n" + 
					"    pro.SEC_PRODUCT     \r\n" + 
					"    ,pro.SEC_LODGING     \r\n" + 
					"    ,lod.NAME as NAME_LODGING\r\n" + 
					"    ,pro.SEC_SPECTACLE   \r\n" + 
					"    ,spe.NAME As NAME_SPECTACLE\r\n" + 
					"    ,pro.SEC_TRANSPORT   \r\n" + 
					"    ,tra.NAME as NAME_TRANSPORT\r\n" + 
					"    ,pro.SEC_CITY_SOURCE \r\n" + 
					"    ,cityS.NAME AS CITY_SOURCE\r\n" + 
					"    ,pro.SEC_CITY_TARGET \r\n" + 
					"    ,cityT.NAME AS CITY_TARGET\r\n" + 
					"    ,pro.NAME            \r\n" + 
					"    ,pro.SPECTACLE_DATE  \r\n" + 
					"    ,pro.ARRIVAL_DATE    \r\n" + 
					"    ,pro.DEPARTURE_DATE  \r\n" + 
					"    ,pro.DESCRIPTION     \r\n" + 
					"    ,pro.CODE            \r\n" + 
					"    ,pro.IMAGE_REF       \r\n" + 
					"from product pro\r\n" + 
					"inner join SPECTACLE spe on pro.SEC_SPECTACLE = pro.SEC_SPECTACLE\r\n" + 
					"inner join LODGING lod on pro.SEC_LODGING = lod.SEC_LODGING\r\n" + 
					"inner join transport tra on pro.sec_transport = tra.SEC_TRANSPORT\r\n" + 
					"inner join CITY cityS  on cityS.SEC_CITY =pro.SEC_CITY_SOURCE\r\n" + 
					"inner join CITY cityT  on cityT.SEC_CITY =pro.SEC_CITY_TARGET\r\n" + 
					"where pro.code='"+unCode+"'";
		
		System.out.println(strQuery);
		try {
			if (contApplList == null)
				contApplList = new ContextAplicationListener();

			BasicDataSource dbs;
			
			dbs = contApplList.getDs();
			
			//dbs.getConnection();
			Connection conn = dbs.getConnection();
			
			
			PreparedStatement pstmt = conn.prepareStatement(strQuery);
			//pstmt.executeQuery();
			ResultSet rs = pstmt.executeQuery();
			
			while (rs.next()) {
				product = new Product();
				
				product.setSecProduct(rs.getLong("SEC_PRODUCT"));
				product.setLodging(new Lodging());
				product.getLodging().setSecLodging(rs.getLong("SEC_LODGING"));
				product.getLodging().setName(rs.getString("NAME_LODGING"));
				
				product.setSpectacle(new Spectacle());
				product.getSpectacle().setSecSpectacle(rs.getLong("SEC_SPECTACLE"));
				product.getSpectacle().setName(rs.getString("NAME_SPECTACLE"));
				
				product.setTransport(new Transport());
				product.getTransport().setSecTransport(rs.getLong("SEC_TRANSPORT"));
				product.getTransport().setName(rs.getString("NAME_TRANSPORT"));
				
				product.setCitySource(new City());
				product.getCitySource().setSecCity(rs.getLong("SEC_CITY_SOURCE"));
				product.getCitySource().setName(rs.getString("CITY_SOURCE"));
				
				product.setCityTarget(new City());
				product.getCityTarget().setSecCity(rs.getLong("SEC_CITY_TARGET"));
				product.getCityTarget().setName(rs.getString("CITY_TARGET"));
				product.setName(rs.getString("NAME"));
				product.setSpectacleDate(rs.getDate("SPECTACLE_DATE"));
				product.setArrivalDate(rs.getDate("ARRIVAL_DATE"));
				product.setDepartureDate(rs.getDate("DEPARTURE_DATE"));
				product.setDescription(rs.getString("DESCRIPTION"));
				product.setCode(rs.getString("CODE"));
				
						
				
				lstProduct.add(product);
		      }
			
			
			conn.close();
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

		
		
		return lstProduct;
		
	}
	
	
	@GET
	@Path("/getProductByNameSpectacle")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public List<Product> getProductByNameSpectacle(@QueryParam("nameSpectacle") 
													String unNameSpectacle){
		System.out.println("Ejecutando metodo[getProductByCode]");
		lstProduct = new ArrayList<Product>();
		
		strQuery = "select \r\n" + 
					"    pro.SEC_PRODUCT     \r\n" + 
					"    ,pro.SEC_LODGING     \r\n" + 
					"    ,lod.NAME as NAME_LODGING\r\n" + 
					"    ,pro.SEC_SPECTACLE   \r\n" + 
					"    ,spe.NAME As NAME_SPECTACLE\r\n" + 
					"    ,pro.SEC_TRANSPORT   \r\n" + 
					"    ,tra.NAME as NAME_TRANSPORT\r\n" + 
					"    ,pro.SEC_CITY_SOURCE \r\n" + 
					"    ,cityS.NAME AS CITY_SOURCE\r\n" + 
					"    ,pro.SEC_CITY_TARGET \r\n" + 
					"    ,cityT.NAME AS CITY_TARGET\r\n" + 
					"    ,pro.NAME            \r\n" + 
					"    ,pro.SPECTACLE_DATE  \r\n" + 
					"    ,pro.ARRIVAL_DATE    \r\n" + 
					"    ,pro.DEPARTURE_DATE  \r\n" + 
					"    ,pro.DESCRIPTION     \r\n" + 
					"    ,pro.CODE            \r\n" + 
					"    ,pro.IMAGE_REF       \r\n" + 
					"from product pro\r\n" + 
					"inner join SPECTACLE spe on pro.SEC_SPECTACLE = pro.SEC_SPECTACLE\r\n" + 
					"inner join LODGING lod on pro.SEC_LODGING = lod.SEC_LODGING\r\n" + 
					"inner join transport tra on pro.sec_transport = tra.SEC_TRANSPORT\r\n" + 
					"inner join CITY cityS  on cityS.SEC_CITY =pro.SEC_CITY_SOURCE\r\n" + 
					"inner join CITY cityT  on cityT.SEC_CITY =pro.SEC_CITY_TARGET\r\n" + 
					"where REGEXP_LIKE (upper(spe.NAME), upper('"+unNameSpectacle+"'))"
							+ "and rownum<100";
		
		System.out.println(strQuery);
		try {
			if (contApplList == null)
				contApplList = new ContextAplicationListener();

			BasicDataSource dbs;
			
			dbs = contApplList.getDs();
			
			//dbs.getConnection();
			Connection conn = dbs.getConnection();
			
			
			PreparedStatement pstmt = conn.prepareStatement(strQuery);
			//pstmt.executeQuery();
			ResultSet rs = pstmt.executeQuery();
			
			while (rs.next()) {
				product = new Product();
				
				product.setSecProduct(rs.getLong("SEC_PRODUCT"));
				product.setLodging(new Lodging());
				product.getLodging().setSecLodging(rs.getLong("SEC_LODGING"));
				product.getLodging().setName(rs.getString("NAME_LODGING"));
				
				product.setSpectacle(new Spectacle());
				product.getSpectacle().setSecSpectacle(rs.getLong("SEC_SPECTACLE"));
				product.getSpectacle().setName(rs.getString("NAME_SPECTACLE"));
				
				product.setTransport(new Transport());
				product.getTransport().setSecTransport(rs.getLong("SEC_TRANSPORT"));
				product.getTransport().setName(rs.getString("NAME_TRANSPORT"));
				
				product.setCitySource(new City());
				product.getCitySource().setSecCity(rs.getLong("SEC_CITY_SOURCE"));
				product.getCitySource().setName(rs.getString("CITY_SOURCE"));
				
				product.setCityTarget(new City());
				product.getCityTarget().setSecCity(rs.getLong("SEC_CITY_TARGET"));
				product.getCityTarget().setName(rs.getString("CITY_TARGET"));
				product.setName(rs.getString("NAME"));
				product.setSpectacleDate(rs.getDate("SPECTACLE_DATE"));
				product.setArrivalDate(rs.getDate("ARRIVAL_DATE"));
				product.setDepartureDate(rs.getDate("DEPARTURE_DATE"));
				product.setDescription(rs.getString("DESCRIPTION"));
				product.setCode(rs.getString("CODE"));
				
						
				
				lstProduct.add(product);
		      }
			
			
			conn.close();
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

		
		
		return lstProduct;
		
	}
	
	
	@GET
	@Path("/getProductByDescription")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public List<Product> getProductByDescription(@QueryParam("description") 
													String unDescription){
		System.out.println("Ejecutando metodo[getProductByDescription]");
		lstProduct = new ArrayList<Product>();
		
		strQuery = "select \r\n" + 
					"    pro.SEC_PRODUCT     \r\n" + 
					"    ,pro.SEC_LODGING     \r\n" + 
					"    ,lod.NAME as NAME_LODGING\r\n" + 
					"    ,pro.SEC_SPECTACLE   \r\n" + 
					"    ,spe.NAME As NAME_SPECTACLE\r\n" + 
					"    ,pro.SEC_TRANSPORT   \r\n" + 
					"    ,tra.NAME as NAME_TRANSPORT\r\n" + 
					"    ,pro.SEC_CITY_SOURCE \r\n" + 
					"    ,cityS.NAME AS CITY_SOURCE\r\n" + 
					"    ,pro.SEC_CITY_TARGET \r\n" + 
					"    ,cityT.NAME AS CITY_TARGET\r\n" + 
					"    ,pro.NAME            \r\n" + 
					"    ,pro.SPECTACLE_DATE  \r\n" + 
					"    ,pro.ARRIVAL_DATE    \r\n" + 
					"    ,pro.DEPARTURE_DATE  \r\n" + 
					"    ,pro.DESCRIPTION     \r\n" + 
					"    ,pro.CODE            \r\n" + 
					"    ,pro.IMAGE_REF       \r\n" + 
					"from product pro\r\n" + 
					"inner join SPECTACLE spe on pro.SEC_SPECTACLE = pro.SEC_SPECTACLE\r\n" + 
					"inner join LODGING lod on pro.SEC_LODGING = lod.SEC_LODGING\r\n" + 
					"inner join transport tra on pro.sec_transport = tra.SEC_TRANSPORT\r\n" + 
					"inner join CITY cityS  on cityS.SEC_CITY =pro.SEC_CITY_SOURCE\r\n" + 
					"inner join CITY cityT  on cityT.SEC_CITY =pro.SEC_CITY_TARGET\r\n" + 
					"where REGEXP_LIKE (upper(pro.DESCRIPTION), upper('"+unDescription+"'))"
							+ "and rownum<100";
		
		System.out.println(strQuery);
		try {
			if (contApplList == null)
				contApplList = new ContextAplicationListener();

			BasicDataSource dbs;
			
			dbs = contApplList.getDs();
			
			//dbs.getConnection();
			Connection conn = dbs.getConnection();
			
			
			PreparedStatement pstmt = conn.prepareStatement(strQuery);
			//pstmt.executeQuery();
			ResultSet rs = pstmt.executeQuery();
			
			while (rs.next()) {
				product = new Product();
				
				product.setSecProduct(rs.getLong("SEC_PRODUCT"));
				product.setLodging(new Lodging());
				product.getLodging().setSecLodging(rs.getLong("SEC_LODGING"));
				product.getLodging().setName(rs.getString("NAME_LODGING"));
				
				product.setSpectacle(new Spectacle());
				product.getSpectacle().setSecSpectacle(rs.getLong("SEC_SPECTACLE"));
				product.getSpectacle().setName(rs.getString("NAME_SPECTACLE"));
				
				product.setTransport(new Transport());
				product.getTransport().setSecTransport(rs.getLong("SEC_TRANSPORT"));
				product.getTransport().setName(rs.getString("NAME_TRANSPORT"));
				
				product.setCitySource(new City());
				product.getCitySource().setSecCity(rs.getLong("SEC_CITY_SOURCE"));
				product.getCitySource().setName(rs.getString("CITY_SOURCE"));
				
				product.setCityTarget(new City());
				product.getCityTarget().setSecCity(rs.getLong("SEC_CITY_TARGET"));
				product.getCityTarget().setName(rs.getString("CITY_TARGET"));
				product.setName(rs.getString("NAME"));
				product.setSpectacleDate(rs.getDate("SPECTACLE_DATE"));
				product.setArrivalDate(rs.getDate("ARRIVAL_DATE"));
				product.setDepartureDate(rs.getDate("DEPARTURE_DATE"));
				product.setDescription(rs.getString("DESCRIPTION"));
				product.setCode(rs.getString("CODE"));
				
						
				
				lstProduct.add(product);
		      }
			
			
			conn.close();
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

		
		
		return lstProduct;
		
	}
	
	
	@GET
	@Path("/getProductByTopVendidos")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public List<Product> getProductByTopVendidos(){
		System.out.println("Ejecutando metodo[getProductByTopVendidos]");
		lstProduct = new ArrayList<Product>();
		
		strQuery = "select \r\n" + 
				"     pro.SEC_PRODUCT     \r\n" + 
				"    ,pro.SEC_LODGING     \r\n" + 
				"    ,lod.NAME as NAME_LODGING\r\n" + 
				"    ,pro.SEC_SPECTACLE   \r\n" + 
				"    ,spe.NAME As NAME_SPECTACLE\r\n" + 
				"    ,pro.SEC_TRANSPORT   \r\n" + 
				"    ,tra.NAME as NAME_TRANSPORT\r\n" + 
				"    ,pro.SEC_CITY_SOURCE \r\n" + 
				"    ,cityS.NAME AS CITY_SOURCE\r\n" + 
				"    ,pro.SEC_CITY_TARGET \r\n" + 
				"    ,cityT.NAME AS CITY_TARGET\r\n" + 
				"    ,pro.NAME            \r\n" + 
				"    ,pro.SPECTACLE_DATE  \r\n" + 
				"    ,pro.ARRIVAL_DATE    \r\n" + 
				"    ,pro.DEPARTURE_DATE  \r\n" + 
				"    ,pro.DESCRIPTION     \r\n" + 
				"    ,pro.CODE            \r\n" + 
				"    ,pro.IMAGE_REF   \r\n" + 
				"   \r\n" + 
				"from product pro\r\n" + 
				"inner join SPECTACLE spe on pro.SEC_SPECTACLE = pro.SEC_SPECTACLE\r\n" + 
				"inner join LODGING lod on pro.SEC_LODGING = lod.SEC_LODGING\r\n" + 
				"inner join transport tra on pro.sec_transport = tra.SEC_TRANSPORT\r\n" + 
				"inner join CITY cityS  on cityS.SEC_CITY =pro.SEC_CITY_SOURCE\r\n" + 
				"inner join CITY cityT  on cityT.SEC_CITY =pro.SEC_CITY_TARGET\r\n" + 
				"where EXISTS (\r\n" + 
				"               SELECT NULL\r\n" + 
				"               FROM toresBalonOrders.order_item orIt\r\n" + 
				"               WHERE orIt.PRODUCT_ID = pro.sec_product\r\n" + 
				"            )\r\n" + 
				"and rownum<6";
		
		System.out.println(strQuery);
		try {
			if (contApplList == null)
				contApplList = new ContextAplicationListener();

			BasicDataSource dbs;
			
			dbs = contApplList.getDs();
			
			//dbs.getConnection();
			Connection conn = dbs.getConnection();
			
			
			PreparedStatement pstmt = conn.prepareStatement(strQuery);
			//pstmt.executeQuery();
			ResultSet rs = pstmt.executeQuery();
			
			while (rs.next()) {
				product = new Product();
				
				product.setSecProduct(rs.getLong("SEC_PRODUCT"));
				product.setLodging(new Lodging());
				product.getLodging().setSecLodging(rs.getLong("SEC_LODGING"));
				product.getLodging().setName(rs.getString("NAME_LODGING"));
				
				product.setSpectacle(new Spectacle());
				product.getSpectacle().setSecSpectacle(rs.getLong("SEC_SPECTACLE"));
				product.getSpectacle().setName(rs.getString("NAME_SPECTACLE"));
				
				product.setTransport(new Transport());
				product.getTransport().setSecTransport(rs.getLong("SEC_TRANSPORT"));
				product.getTransport().setName(rs.getString("NAME_TRANSPORT"));
				
				product.setCitySource(new City());
				product.getCitySource().setSecCity(rs.getLong("SEC_CITY_SOURCE"));
				product.getCitySource().setName(rs.getString("CITY_SOURCE"));
				
				product.setCityTarget(new City());
				product.getCityTarget().setSecCity(rs.getLong("SEC_CITY_TARGET"));
				product.getCityTarget().setName(rs.getString("CITY_TARGET"));
				product.setName(rs.getString("NAME"));
				product.setSpectacleDate(rs.getDate("SPECTACLE_DATE"));
				product.setArrivalDate(rs.getDate("ARRIVAL_DATE"));
				product.setDepartureDate(rs.getDate("DEPARTURE_DATE"));
				product.setDescription(rs.getString("DESCRIPTION"));
				product.setCode(rs.getString("CODE"));
				
						
				
				lstProduct.add(product);
		      }
			
			
			conn.close();
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

		
		
		return lstProduct;
		
	}
	
	
	@GET
	@Path("/getProductBySecProduct")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public List<Product> getProductBySecProduct(@QueryParam("secProduct") 
												String unSecProduc){
		System.out.println("Ejecutando metodo[getProductBySecProduct]");
		lstProduct = new ArrayList<Product>();
		
		
		strQuery = "select \r\n" + 
					"    pro.SEC_PRODUCT     \r\n" + 
					"    ,pro.SEC_LODGING     \r\n" + 
					"    ,lod.NAME as NAME_LODGING\r\n" + 
					"    ,pro.SEC_SPECTACLE   \r\n" + 
					"    ,spe.NAME As NAME_SPECTACLE\r\n" + 
					"    ,pro.SEC_TRANSPORT   \r\n" + 
					"    ,tra.NAME as NAME_TRANSPORT\r\n" + 
					"    ,pro.SEC_CITY_SOURCE \r\n" + 
					"    ,cityS.NAME AS CITY_SOURCE\r\n" + 
					"    ,pro.SEC_CITY_TARGET \r\n" + 
					"    ,cityT.NAME AS CITY_TARGET\r\n" + 
					"    ,pro.NAME            \r\n" + 
					"    ,pro.SPECTACLE_DATE  \r\n" + 
					"    ,pro.ARRIVAL_DATE    \r\n" + 
					"    ,pro.DEPARTURE_DATE  \r\n" + 
					"    ,pro.DESCRIPTION     \r\n" + 
					"    ,pro.CODE            \r\n" + 
					"    ,pro.IMAGE_REF       \r\n" + 
					"from product pro\r\n" + 
					"inner join SPECTACLE spe on pro.SEC_SPECTACLE = pro.SEC_SPECTACLE\r\n" + 
					"inner join LODGING lod on pro.SEC_LODGING = lod.SEC_LODGING\r\n" + 
					"inner join transport tra on pro.sec_transport = tra.SEC_TRANSPORT\r\n" + 
					"inner join CITY cityS  on cityS.SEC_CITY =pro.SEC_CITY_SOURCE\r\n" + 
					"inner join CITY cityT  on cityT.SEC_CITY =pro.SEC_CITY_TARGET\r\n" + 
					"where pro.SEC_PRODUCT='"+unSecProduc+"'";
		
		System.out.println(strQuery);
		try {
			if (contApplList == null)
				contApplList = new ContextAplicationListener();

			BasicDataSource dbs;
			
			dbs = contApplList.getDs();
			
			//dbs.getConnection();
			Connection conn = dbs.getConnection();
			
			
			PreparedStatement pstmt = conn.prepareStatement(strQuery);
			//pstmt.executeQuery();
			ResultSet rs = pstmt.executeQuery();
			
			while (rs.next()) {
				product = new Product();
				//System.out.println("[615]");
				product.setSecProduct(rs.getLong("SEC_PRODUCT"));
				product.setLodging(new Lodging());
				product.getLodging().setSecLodging(rs.getLong("SEC_LODGING"));
				product.getLodging().setName(rs.getString("NAME_LODGING"));
				product.getLodging().setCost(new Long(1000));
				product.setSpectacle(new Spectacle());
				product.getSpectacle().setSecSpectacle(rs.getLong("SEC_SPECTACLE"));
				product.getSpectacle().setCost(new Long(1500));
				product.getSpectacle().setName(rs.getString("NAME_SPECTACLE"));
				
				product.setTransport(new Transport());
				product.getTransport().setSecTransport(rs.getLong("SEC_TRANSPORT"));
				product.getTransport().setName(rs.getString("NAME_TRANSPORT"));
				product.getTransport().setCost(new Long(800));
				product.setCitySource(new City());
				product.getCitySource().setSecCity(rs.getLong("SEC_CITY_SOURCE"));
				product.getCitySource().setName(rs.getString("CITY_SOURCE"));
				
				product.setCityTarget(new City());
				product.getCityTarget().setSecCity(rs.getLong("SEC_CITY_TARGET"));
				product.getCityTarget().setName(rs.getString("CITY_TARGET"));
				product.setName(rs.getString("NAME"));
				product.setSpectacleDate(rs.getDate("SPECTACLE_DATE"));
				product.setArrivalDate(rs.getDate("ARRIVAL_DATE"));
				product.setDepartureDate(rs.getDate("DEPARTURE_DATE"));
				product.setDescription(rs.getString("DESCRIPTION"));
				product.setCode(rs.getString("CODE"));
				
				//System.out.println("tamaño lista[]"+ lstProduct.size());		
				
				lstProduct.add(product);
		      }
			
			
			conn.close();
			
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("[6543]");
			e.printStackTrace();
		}

		
		System.out.println("[658]");
		return lstProduct;
		
	}
	
	
	
	@GET
	@Path("/getProductCampBySecCamp")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public List<Product> getProductCampBySecCamp(@QueryParam("secCampana") 
												String secCampana){
		System.out.println("Ejecutando metodo[getProductCampBySecCamp]");
		lstProduct = new ArrayList<Product>();
		
		
		strQuery = "select \r\n" + 
				"    pro.SEC_PRODUCT     \r\n" + 
				"    ,pro.SEC_LODGING     \r\n" + 
				"    ,lod.NAME as NAME_LODGING\r\n" + 
				"    ,pro.SEC_SPECTACLE   \r\n" + 
				"    ,spe.NAME As NAME_SPECTACLE\r\n" + 
				"    ,pro.SEC_TRANSPORT   \r\n" + 
				"    ,tra.NAME as NAME_TRANSPORT\r\n" + 
				"    ,pro.SEC_CITY_SOURCE \r\n" + 
				"    ,cityS.NAME AS CITY_SOURCE\r\n" + 
				"    ,pro.SEC_CITY_TARGET \r\n" + 
				"    ,cityT.NAME AS CITY_TARGET\r\n" + 
				"    ,pro.NAME            \r\n" + 
				"    ,pro.SPECTACLE_DATE  \r\n" + 
				"    ,pro.ARRIVAL_DATE    \r\n" + 
				"    ,pro.DEPARTURE_DATE  \r\n" + 
				"    ,pro.DESCRIPTION     \r\n" + 
				"    ,pro.CODE            \r\n" + 
				"    ,pro.IMAGE_REF       \r\n" + 
				"from product pro\r\n" + 
				"inner join SPECTACLE spe on pro.SEC_SPECTACLE = pro.SEC_SPECTACLE\r\n" + 
				"inner join LODGING lod on pro.SEC_LODGING = lod.SEC_LODGING\r\n" + 
				"inner join transport tra on pro.sec_transport = tra.SEC_TRANSPORT\r\n" + 
				"inner join CITY cityS  on cityS.SEC_CITY =pro.SEC_CITY_SOURCE\r\n" + 
				"inner join CITY cityT  on cityT.SEC_CITY =pro.SEC_CITY_TARGET\r\n" + 
				"where EXISTS (\r\n" + 
				"              select null \r\n" + 
				"              from detail_campana dc \r\n" + 
				"              where pro.SEC_PRODUCT = dc.SEC_PRODUCT\r\n" + 
				"              and dc.sec_campana = \r" + secCampana + 
				"              )";
		
		System.out.println(strQuery);
		try {
			if (contApplList == null)
				contApplList = new ContextAplicationListener();

			BasicDataSource dbs;
			
			dbs = contApplList.getDs();
			
			//dbs.getConnection();
			Connection conn = dbs.getConnection();
			
			
			PreparedStatement pstmt = conn.prepareStatement(strQuery);
			//pstmt.executeQuery();
			ResultSet rs = pstmt.executeQuery();
			
			while (rs.next()) {
				product = new Product();
				//System.out.println("[615]");
				product.setSecProduct(rs.getLong("SEC_PRODUCT"));
				product.setLodging(new Lodging());
				product.getLodging().setSecLodging(rs.getLong("SEC_LODGING"));
				product.getLodging().setName(rs.getString("NAME_LODGING"));
				product.getLodging().setCost(new Long(1000));
				product.setSpectacle(new Spectacle());
				product.getSpectacle().setSecSpectacle(rs.getLong("SEC_SPECTACLE"));
				product.getSpectacle().setCost(new Long(1500));
				product.getSpectacle().setName(rs.getString("NAME_SPECTACLE"));
				//System.out.println("[625]");
				product.setTransport(new Transport());
				product.getTransport().setSecTransport(rs.getLong("SEC_TRANSPORT"));
				product.getTransport().setName(rs.getString("NAME_TRANSPORT"));
				product.getTransport().setCost(new Long(800));
				product.setCitySource(new City());
				product.getCitySource().setSecCity(rs.getLong("SEC_CITY_SOURCE"));
				product.getCitySource().setName(rs.getString("CITY_SOURCE"));
				//System.out.println("[633]");
				product.setCityTarget(new City());
				product.getCityTarget().setSecCity(rs.getLong("SEC_CITY_TARGET"));
				product.getCityTarget().setName(rs.getString("CITY_TARGET"));
				product.setName(rs.getString("NAME"));
				product.setSpectacleDate(rs.getDate("SPECTACLE_DATE"));
				product.setArrivalDate(rs.getDate("ARRIVAL_DATE"));
				product.setDepartureDate(rs.getDate("DEPARTURE_DATE"));
				product.setDescription(rs.getString("DESCRIPTION"));
				product.setCode(rs.getString("CODE"));
				//System.out.println("[643]");
				//System.out.println("tamaño lista[]"+ lstProduct.size());		
				
				lstProduct.add(product);
		      }
			
			
			conn.close();
			
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("[6543]");
			e.printStackTrace();
		}

		
		//System.out.println("[658]");
		return lstProduct;
		
	}
	
	
	
	
	@GET
	@Path("/getProductByName")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public List<Product> getProductByName(@QueryParam("name") 
													String unName){
		System.out.println("Ejecutando metodo[getProductByCode]");
		lstProduct = new ArrayList<Product>();
		
		strQuery = "select \r\n" + 
					"    pro.SEC_PRODUCT     \r\n" + 
					"    ,pro.SEC_LODGING     \r\n" + 
					"    ,lod.NAME as NAME_LODGING\r\n" + 
					"    ,pro.SEC_SPECTACLE   \r\n" + 
					"    ,spe.NAME As NAME_SPECTACLE\r\n" + 
					"    ,pro.SEC_TRANSPORT   \r\n" + 
					"    ,tra.NAME as NAME_TRANSPORT\r\n" + 
					"    ,pro.SEC_CITY_SOURCE \r\n" + 
					"    ,cityS.NAME AS CITY_SOURCE\r\n" + 
					"    ,pro.SEC_CITY_TARGET \r\n" + 
					"    ,cityT.NAME AS CITY_TARGET\r\n" + 
					"    ,pro.NAME            \r\n" + 
					"    ,pro.SPECTACLE_DATE  \r\n" + 
					"    ,pro.ARRIVAL_DATE    \r\n" + 
					"    ,pro.DEPARTURE_DATE  \r\n" + 
					"    ,pro.DESCRIPTION     \r\n" + 
					"    ,pro.CODE            \r\n" + 
					"    ,pro.IMAGE_REF       \r\n" + 
					"from product pro\r\n" + 
					"inner join SPECTACLE spe on pro.SEC_SPECTACLE = pro.SEC_SPECTACLE\r\n" + 
					"inner join LODGING lod on pro.SEC_LODGING = lod.SEC_LODGING\r\n" + 
					"inner join transport tra on pro.sec_transport = tra.SEC_TRANSPORT\r\n" + 
					"inner join CITY cityS  on cityS.SEC_CITY =pro.SEC_CITY_SOURCE\r\n" + 
					"inner join CITY cityT  on cityT.SEC_CITY =pro.SEC_CITY_TARGET\r\n" + 
					"where REGEXP_LIKE (upper(pro.NAME), upper('"+unName+"'))"
							+ "and rownum<100";
		
		System.out.println(strQuery);
		try {
			if (contApplList == null)
				contApplList = new ContextAplicationListener();

			BasicDataSource dbs;
			
			dbs = contApplList.getDs();
			
			//dbs.getConnection();
			Connection conn = dbs.getConnection();
			
			
			PreparedStatement pstmt = conn.prepareStatement(strQuery);
			//pstmt.executeQuery();
			ResultSet rs = pstmt.executeQuery();
			
			while (rs.next()) {
				product = new Product();
				
				product.setSecProduct(rs.getLong("SEC_PRODUCT"));
				product.setLodging(new Lodging());
				product.getLodging().setSecLodging(rs.getLong("SEC_LODGING"));
				product.getLodging().setName(rs.getString("NAME_LODGING"));
				
				product.setSpectacle(new Spectacle());
				product.getSpectacle().setSecSpectacle(rs.getLong("SEC_SPECTACLE"));
				product.getSpectacle().setName(rs.getString("NAME_SPECTACLE"));
				
				product.setTransport(new Transport());
				product.getTransport().setSecTransport(rs.getLong("SEC_TRANSPORT"));
				product.getTransport().setName(rs.getString("NAME_TRANSPORT"));
				
				product.setCitySource(new City());
				product.getCitySource().setSecCity(rs.getLong("SEC_CITY_SOURCE"));
				product.getCitySource().setName(rs.getString("CITY_SOURCE"));
				
				product.setCityTarget(new City());
				product.getCityTarget().setSecCity(rs.getLong("SEC_CITY_TARGET"));
				product.getCityTarget().setName(rs.getString("CITY_TARGET"));
				product.setName(rs.getString("NAME"));
				product.setSpectacleDate(rs.getDate("SPECTACLE_DATE"));
				product.setArrivalDate(rs.getDate("ARRIVAL_DATE"));
				product.setDepartureDate(rs.getDate("DEPARTURE_DATE"));
				product.setDescription(rs.getString("DESCRIPTION"));
				product.setCode(rs.getString("CODE"));
				
						
				
				lstProduct.add(product);
		      }
			
			
			conn.close();
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

		
		
		return lstProduct;
		
	}
	


}
